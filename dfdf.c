#include "dfdf.h"
#include "uart.h"
#include "pff.h"
bdata uchar spidw,spidr;
sbit d0=spidw^0;
sbit d1=spidw^1;
sbit d2=spidw^2;
sbit d3=spidw^3;
sbit d4=spidw^4;
sbit d5=spidw^5;
sbit d6=spidw^6;
sbit d7=spidw^7;
sbit c0=spidr^0;
sbit c1=spidr^1;
sbit c2=spidr^2;
sbit c3=spidr^3;
sbit c4=spidr^4;
sbit c5=spidr^5;
sbit c6=spidr^6;
sbit c7=spidr^7;

sbit cs=P3^4;
sbit spi_do=P3^5;
sbit spi_di=P3^7;
sbit clk=P3^6;
uchar SDtype; //0代表没有卡，1代表sd1.0卡，2代表sd2.0普通卡，3代表mmc卡,4代表v2.0高容量卡，。
uint sss;
//发送数据，delay是否延时

uchar spiwr(uchar mm,uchar delay)     //模式4上升沿采样
{
	spidw=mm;
    if(delay)yanshi(10);
    clk=0;spi_do=d7;  	if(delay)yanshi(10);
    clk=1;c7=spi_di;    if(delay)yanshi(10);
    clk=0;spi_do=d6;  	if(delay)yanshi(10);
    clk=1;c6=spi_di;    if(delay)yanshi(10);
    clk=0;spi_do=d5;  	if(delay)yanshi(10);
    clk=1;c5=spi_di;    if(delay)yanshi(10);
    clk=0;spi_do=d4;  	if(delay)yanshi(10);
    clk=1;c4=spi_di;    if(delay)yanshi(10);
    clk=0;spi_do=d3;  	if(delay)yanshi(10);
    clk=1;c3=spi_di;    if(delay)yanshi(10);
    clk=0;spi_do=d2;  	if(delay)yanshi(10);
    clk=1;c2=spi_di;    if(delay)yanshi(10);
    clk=0;spi_do=d1;  	if(delay)yanshi(10);
    clk=1;c1=spi_di;    if(delay)yanshi(10);
    clk=0;spi_do=d0;  	if(delay)yanshi(10);
    clk=1;c0=spi_di;
	return spidr;         
}                     
                                           
uchar sdw(uchar com,u32 d,uchar crc,uchar delay)
{
	uchar k;
    uchar times=255;
	cs=0;

	while((spiwr(0xff,delay)!=0xff)&&--times);//判断忙
    if(times==0)
    {
        return 0xff;
	}
    send_byte(0x0d);
    send_byte(0x0a);
    send_byte('c');
    txhex(com,0xff);
    spiwr(0x40|com,delay);
	spiwr(d>>24,delay);
	spiwr(d>>16,delay);
	spiwr(d>>8,delay);
	spiwr(d,delay);
	spiwr(crc,delay);
	times=20;
	do{
		k=spiwr(0xff,delay);
	}
	while((k&0x80)&&(times--));  //读响应
	return k;
}

uchar sd_init()
{
	uchar q,times,i;
    uchar buf[4];
    clk=1;//spi模式4，高电平为空闲
    
	for(q=0;q<10;q++)//发送80个时钟
	{
		spiwr(0xff,1);
	}
    times=20;
	do
	{
		q=sdw(CMD0,0,0x95,1);//接收R1响应，看是否空闲
	}
	while(q!=0x01&&--times);  //进入spi模式 条件：片选为低电平，发送cmd0

    if(q==0x01)
    {
        q=sdw(CMD8,0x1AA,0x87,1);  //CMD8判断是否有响应。有则2.0，无则1.0或mmc
        //cmd8返回值为R7响应

        if(q==1) 
        {
            SDtype=2;//证明v2.0卡的类型
            times=255;
            do{
                sdw(CMD55,0,1,1);
                q=sdw(CMD41,0x40000000,1,1);
            }while(q&&--times);
            if(times&&sdw(CMD58,0,1,1)==0)
            {
                for(i=0;i<4;i++)//sd卡返回的数据先发高位再发低位。
                {
                    buf[i]=spiwr(0xff,1);
                }
                if(buf[0]&0x40)
                {
                    SDtype=4;
                }
                else
                {
                    SDtype=2;
                }
            }
        }
        else  
        {	    //证明是否v1.0或mmc，仿真以确定sd卡，在此不做证明mmc (mmc对cmd55和acmd41无效)
                SDtype=1;
                //暂时先不适配了
//             sdw(CMD55,0,1);
//             q=sdw(CMD41,0x40000000,1);
//             if(q<=1)
//             {
//                do
//                {
//                    sdw(CMD55,0,1);
//                    q=sdw(CMD41,0x40000000,1);
//                }
//                while(q==1);

//                q=sdw(16,512,1); //分512字节数据块 R1响应
//             }
//            do
//            {
//                sdw(CMD55,0,1);
//                q=sdw(CMD41,0x40000000,1);
//            }
//            while(q==1);

//            q=sdw(16,512,1); //分512字节数据块 R1响应
        } 
    }
    else
    {
        SDtype=0;//没有卡插上
    }
    txprint("sd：",0);
    send_byte('0'+SDtype);
    send_byte(0x0d);
    send_byte(0x0a);	
    return SDtype;
}

//void xiez(uchar lp)
//{ 
//	uint q;
//	spiwr(lp);

//	for(q=0;q<512;q++)
//	spiwr(0x00);   //s++;
// 
//	spiwr(0xff);  //发伪crc校验
//	spiwr(0xff); 
//	spiwr(0xff); //最后sd会发0x05给单片机

//}

//void xie(u32 adds,uchar cou)
//{ 
//	if(cou==1)
//	{
//		sdw(24,adds,1);
//		xiez(0xfe);
//	}
//	else 
//	{
//		sdw(55,0,1);
//		sdw(23,cou,1);
//		sdw(25,adds,1);
//		while(cou!=0)
//		{		
//			xiez(0xfc);
//			//s+=512;
//			cou--;
//		}
//		spiwr(0xfd);  
//	}
//}


uint duz(
    uchar* buff,		/* Pointer to the destination object */
	uint offset,	/* Offset in the sector */
	uint count		/* Byte count (bit15:destination) */
)
{
	uint q,count1=count;
    uchar times=100;
	while((spiwr(0xff,0)!=0xfe)&&(--times));
    if(times==0)
    {
        return count;//表示失败
    }
    txprint("\XD\X0A begin!!!!",1);
	for(q=0;q<offset;q++)
	{

        spiwr(0xff,0);
	}
    for(q;q<(offset+count)&&q<512;q++)
	{

        *buff=spiwr(0xff,0);
        buff++;
        count1--;
	}
    for(q;q<512;q++)
	{

        spiwr(0xff,0);
	}
    send_byte('d');
    txhex(spiwr(0xff,0),spiwr(0xff,0));
//	spiwr(0xff,0); //接收伪crc校验
//	spiwr(0xff,0);
    return count1;//表示成功
}

//读块函数
//adds为地址 cou为读多少块
uchar read_block(
	uchar* buff,		/* Pointer to the destination object */
	u32 sector,	/* Sector number (LBA) */
	uint offset,	/* Offset in the sector */
	uint count		/* Byte count (bit15:destination) */
)
{
    uint q;
    if(offset+count<=512)
    {

        q=sdw(CMD17,sector,1,0);
        if(q==0)
        {
            duz(buff,offset,count);
        }
    }
    else
    {
            q=sdw(CMD18,sector,1,0);
            do
            {
                q = duz(buff,offset,count);
                buff += (count-q);
                count = q;
                offset = 0;

            }while(q!=0);
            sdw(CMD12,0,1,0);
    }
    return 0;
}

void io_init()
{
    P3M0=0XF0;
    P3M1=0XF0;
}
void main()
{
    uchar mmss[13]={0};
    FRESULT ret;
    uint aa=12,bb;
    FATFS sdfs;
    io_init();
	uart_init();
    txprint("\x0d\x0aread test！！！！----",1);
    ret=pf_mount(&sdfs);
    txprint("pf_mount",1);
    txhex(ret,0x01);
    ret=pf_open("nihao.txt");
    txprint("pf_open",1);
    txhex(ret,0x02);

    ret=pf_read (mmss, aa, &bb);
    txprint("pf_read",1);
    txhex(ret,0x03);
    txprint(mmss,1);

    while(1);
//	sdw(9,0,1);
//	duz(16);
//	//xie(512,3);
//	du(0,3);

}


