#ifndef _51UART_H_
#define _51UART_H_
#include "head.h"

void uart_init();
uchar receive_byte();
void send_byte(uchar uartData);
void txprint(uchar *str,uchar nr);
void txhex(uchar thex,uchar rhex);

#endif