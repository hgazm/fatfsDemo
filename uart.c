#include "uart.h"
uchar hexstr[4]={0,0,'H',0};
void uart_init()
{
    SCON=0X50;
    BRT=0XFD;
    TI=1;
    AUXR=0x15;
}

//等待接收一个字节
uchar receive_byte()
{
    while(RI==0);
    RI=0;
    return SBUF;
}
//等待发送一个字节
void send_byte(uchar uartData)
{
    while(!TI);
    TI=0;
	SBUF=uartData;
}

void txprint(uchar *str,uchar nr)
{
	//串口发送,nr表示是否加换行
	while(*str)
	{
		send_byte(*str);
		str++;
	}
	if(nr)
	{	
		send_byte(0x0d);
		send_byte(0x0a);	
	}
}

void txhex(uchar thex,uchar rhex)
{
	//串口发送,nr表示是否加换行
    uchar t;

    send_byte('T');//发送
    send_byte(':');
    t=thex&0x0f;
    
    if(t>9)
    {
        hexstr[1]=t-10+'A';
    }
    else
    {
        hexstr[1]=t+'0';
    }
	t=thex>>4;
    if(t>9)
    {
        hexstr[0]=t-10+'A';
    }
    else
    {
        hexstr[0]=t+'0';
    }
    txprint(hexstr,0);
    send_byte(' ');
    send_byte('R');//发送
    send_byte(':');
    t=rhex&0x0f;
    
    if(t>9)
    {
        hexstr[1]=t-10+'A';
    }
    else
    {
        hexstr[1]=t+'0';
    }
	t=rhex>>4;
    if(t>9)
    {
        hexstr[0]=t-10+'A';
    }
    else
    {
        hexstr[0]=t+'0';
    }
    txprint(hexstr,0);
    send_byte(' ');

}