#ifndef __DFDF_H_
#define __DFDF_H_
#include "head.h"
//RI 7-0
//高位为0 6：参数错误 5地址错误 4擦出序列错误 3crc错误 2非法命令 1擦除复位 0空闲状态

#define CMD0  0X00   //R1   复位SD卡
#define CMD8  0X08   //R7   发送接口状态命令
#define CMD9  0X09   //R1   读取卡特定数据寄存器
#define CMD10 0X0a   //R1   读取卡标志数据寄存器
#define CMD12 0X0c 
#define CMD16 0X10   //R1   设置块大小
#define CMD17 0X11   //R1   读取一块数据
#define CMD18 0X12   //R1   读取一块数据
#define CMD24 0X18   //R1   写入一块数据
#define CMD41 0X29   //R1   发送主机容量支持信息
#define CMD55 0X37   //R1   告诉sd卡下个命令是特定应用命令
#define CMD58 0X3a   //R3   读取ocr寄存器

uchar sd_init();

uchar read_block(
	uchar* buff,		/* Pointer to the destination object */
	u32 sector,	/* Sector number (LBA) */
	uint offset,	/* Offset in the sector */
	uint count		/* Byte count (bit15:destination) */
);
#endif